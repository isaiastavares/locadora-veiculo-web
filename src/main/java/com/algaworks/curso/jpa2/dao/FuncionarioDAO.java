package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.Funcionario;
import com.algaworks.curso.jpa2.service.exception.NegocioException;

public class FuncionarioDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	public Funcionario buscarPeloCodigo(Long codigo) {
		return em.find(Funcionario.class, codigo);
	}

	public void excluir(Funcionario funcionario) throws NegocioException {
		funcionario = em.find(Funcionario.class, funcionario.getCodigo());

		em.remove(funcionario);
		em.flush();

	}

	@SuppressWarnings("unchecked")
	public List<Funcionario> buscarTodos() {
		return em.createQuery("from Funcionario").getResultList();
	}

	public void salvar(Funcionario funcionario) {
		em.merge(funcionario);
	}

}
