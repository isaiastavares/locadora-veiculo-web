package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CarroDAO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5804238669540543682L;

	@Inject
	private EntityManager em;

	public Carro buscarPeloCodigo(Long codigo) {
		return em.find(Carro.class, codigo);
	}

	@SuppressWarnings("unchecked")
	public List<Carro> buscarTodos() {
		return em.createQuery("from Carro").getResultList();
	}

	@Transactional
	public void excluir(Carro carro) throws NegocioException{
		carro = this.em.find(Carro.class, carro.getCodigo());

		this.em.remove(carro);
		this.em.flush();
	}

	public void salvar(Carro carro) {
		this.em.merge(carro);
	}

	public Carro buscarCarroComAcessorios(Long codigo) {
		return (Carro) em.createQuery("select c from Carro c JOIN c.acessorios a WHERE c.codigo = ?")
				.setParameter(1, codigo)
				.getSingleResult();
	}

}
