package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.Motorista;
import com.algaworks.curso.jpa2.service.exception.NegocioException;

public class MotoristaDAO implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager em;

	public void excluir(Motorista motorista) throws NegocioException{
		motorista = em.find(Motorista.class, motorista.getCodigo());

		em.remove(motorista);
		em.flush();
	}

	public void salvar(Motorista motorista) throws NegocioException {
		em.merge(motorista);
	}

	@SuppressWarnings("unchecked")
	public List<Motorista> buscarTodos() {
		return em.createQuery("from Motorista").getResultList();
	}

	public Motorista buscarPeloCodigo(Long codigo) {
		return em.find(Motorista.class, codigo);
	}

}
