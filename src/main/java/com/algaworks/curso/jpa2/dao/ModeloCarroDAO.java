package com.algaworks.curso.jpa2.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.curso.jpa2.modelo.ModeloCarro;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class ModeloCarroDAO implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6538089227630233020L;
	@Inject
	private EntityManager em;

	public ModeloCarro buscarPeloCodigo(Long codigo) {
		return em.find(ModeloCarro.class, codigo);
	}

	@SuppressWarnings("unchecked")
	public List<ModeloCarro> buscarTodos() {
		return em.createQuery("from ModeloCarro").getResultList();
	}

	@Transactional
	public void excluir(ModeloCarro modeloCarro) throws NegocioException{
		modeloCarro = em.find(ModeloCarro.class, modeloCarro.getCodigo());

		em.remove(modeloCarro);
		em.flush();
	}

	public void Salvar(ModeloCarro modeloCarro) {
		em.merge(modeloCarro);
	}

}
