package com.algaworks.curso.jpa2.modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.algaworks.curso.jpa2.service.exception.NegocioException;

@Entity
public class Carro {

	private Long codigo;
	private String placa;
	private String chassi;
	private String cor;
	private double valorDiaria;
	private ModeloCarro modelo;
	private List<Acessorio> acessorios;

	private List<Aluguel> alugueis; //retorna a lista de alugueis do carro instanciado

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getChassi() {
		return chassi;
	}
	public void setChassi(String chassi) {
		this.chassi = chassi;
	}

	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}

	public double getValorDiaria() {
		return valorDiaria;
	}
	public void setValorDiaria(double valorDiaria) {
		this.valorDiaria = valorDiaria;
	}

	@ManyToOne
	@JoinColumn(name="codigo_modelo")
	public ModeloCarro getModelo() {
		return modelo;
	}
	public void setModelo(ModeloCarro modelo) {
		this.modelo = modelo;
	}

	@ManyToMany(fetch=FetchType.LAZY) //Colocando so pela Ciencia pois esse eh o valor default para mapeamento Many to Many
	@JoinTable(name="acessorios_do_carro", //Define o nome da tabela de relacionamento many to many
			joinColumns=@JoinColumn(name="codigo_carro"), //Define o nome da foreingkey para essa classe
			inverseJoinColumns=@JoinColumn(name="codigo_acessorio")) //Define o nome da foreingkey para a classe destino (acessorio)
	public List<Acessorio> getAcessorios() {
		return acessorios;
	}
	public void setAcessorios(List<Acessorio> acessorios) {
		this.acessorios = acessorios;
	}

	@OneToMany(mappedBy="carro") //Um carro tem vários alugueis
								 //O parametro mappedBy quer dizer que esse mapeamento já foi feito na entidade Aluguel no atríbuto carro;
	public List<Aluguel> getAlugueis() {
		return alugueis;
	}
	public void setAlugueis(List<Aluguel> alugueis) {
		this.alugueis = alugueis;
	}

	/** Valida se um carro &eacute; valido, ou seja, tem todos os valores preenchidos */
	public boolean isCarroValido(Carro carro) throws NegocioException {
		if(carro.getChassi() == null || carro.getChassi().trim().equals("")) {
			throw new NegocioException("Informe o chassi do carro");
		}

		if(carro.getCor() == null || carro.getCor().trim().equals("")) {
			throw new NegocioException("Informe  a cor do carro");
		}

		if(carro.getModelo() == null) {
			throw new NegocioException("Informe o modelo do carro");
		}

		if(carro.getPlaca() == null || carro.getPlaca().trim().equals("")) {
			throw new NegocioException("Informe a placa do carro");
		}

		if(carro.getValorDiaria() == 0) {
			throw new NegocioException("Informe o valor da diaria e ela não pode ser ZERO");
		}

		return true;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Carro other = (Carro) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}



}
