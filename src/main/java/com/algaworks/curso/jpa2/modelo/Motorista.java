package com.algaworks.curso.jpa2.modelo;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.algaworks.curso.jpa2.service.exception.NegocioException;

@Entity
@DiscriminatorValue("1")
public class Motorista extends Pessoa {

	private String numeroCnh;

	@Column(name="numero_cnh")
	public String getNumeroCnh() {
		return numeroCnh;
	}
	public void setNumeroCnh(String numeroCnh) {
		this.numeroCnh = numeroCnh;
	}
	public static boolean isMotoristaValido(Motorista motorista) throws NegocioException {

		Pessoa.isPessoaValida(motorista);
		if( motorista.getNumeroCnh() == null || motorista.getNumeroCnh().trim().equals("") ) {
			throw new NegocioException("Informe o núemro da CNH");
		}

		return true;
	}

}
