package com.algaworks.curso.jpa2.modelo;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.algaworks.curso.jpa2.service.exception.NegocioException;

@Entity
@DiscriminatorValue("2")
public class Funcionario extends Pessoa {

	private String matricula;

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public static boolean isFuncionarioValido(Funcionario funcionario) throws NegocioException {

		Pessoa.isPessoaValida(funcionario);

		if(funcionario.getMatricula() == null || funcionario.getMatricula().trim().equals("")) {
			throw new NegocioException("Informe a matrícula do funcionario");
		}

		return true;

	}

}
