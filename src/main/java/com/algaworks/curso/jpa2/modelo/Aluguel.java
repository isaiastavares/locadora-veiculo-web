package com.algaworks.curso.jpa2.modelo;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.algaworks.curso.jpa2.service.exception.NegocioException;

@Entity
public class Aluguel {

	private Long codigo;
	private BigDecimal valorTotal;
	private Carro carro;

	private ApoliceSeguro apoliceSeguro;

	private Calendar dataPedido;
	private Date dataEntrega;
	private Date dataDevolucao;

	private Motorista motorista;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getCodigo() {
		return codigo;
	}
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	@ManyToOne // Muitos alugueis tem um carro associado
	@JoinColumn(name="codigo_carro")
	public Carro getCarro() {
		return carro;
	}
	public void setCarro(Carro carro) {
		this.carro = carro;
	}

	@OneToOne(cascade=CascadeType.ALL) //Define mapeamento um para um e faz com que todas as operacoes ocorram em cascata
									   // Dessa forma as apolices de seguro serao salvas antes do aluguel
	@JoinColumn(name="codigo_apolice_seguro")
	public ApoliceSeguro getApoliceSeguro() {
		return apoliceSeguro;
	}
	public void setApoliceSeguro(ApoliceSeguro apoliceSeguro) {
		this.apoliceSeguro = apoliceSeguro;
	}

	@Temporal(TemporalType.DATE)
	public Calendar getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(Calendar dataPedido) {
		this.dataPedido = dataPedido;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getDataDevolucao() {
		return dataDevolucao;
	}
	public void setDataDevolucao(Date dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	@ManyToOne
	public Motorista getMotorista() {
		return motorista;
	}
	public void setMotorista(Motorista motorista) {
		this.motorista = motorista;
	}

	public boolean isAluguelValido(Aluguel aluguel) throws NegocioException {

		if(aluguel.getValorTotal() == null || aluguel.getValorTotal() == BigDecimal.ZERO) {
			throw new NegocioException("Informe um valor para o aluguel");
		}
		if(aluguel.getCarro() == null) {
			throw new NegocioException("Informe o carro que sera alugado");
		}

		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aluguel other = (Aluguel) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

}
