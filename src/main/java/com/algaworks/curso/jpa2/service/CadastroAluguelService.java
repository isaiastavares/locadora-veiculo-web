package com.algaworks.curso.jpa2.service;

import java.io.Serializable;
import java.util.Calendar;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.AluguelDAO;
import com.algaworks.curso.jpa2.modelo.Aluguel;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CadastroAluguelService implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2399074043159358887L;
	@Inject
	private AluguelDAO aluguelDao;

	@Transactional
	public void salvar(Aluguel aluguel) throws NegocioException{
		if(aluguel.isAluguelValido(aluguel)) {
			aluguel.setDataPedido(Calendar.getInstance()); //informa a data atual;
			this.aluguelDao.salvar(aluguel);
		}
	}

}
