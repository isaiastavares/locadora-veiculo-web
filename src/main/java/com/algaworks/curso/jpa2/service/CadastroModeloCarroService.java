package com.algaworks.curso.jpa2.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.ModeloCarroDAO;
import com.algaworks.curso.jpa2.modelo.ModeloCarro;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CadastroModeloCarroService implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3695840627194062320L;
	@Inject
	private ModeloCarroDAO modeloCarroDao;

	@Transactional
	public void Salvar(ModeloCarro modeloCarro) throws NegocioException {

		if(modeloCarro.getDescricao() == null || modeloCarro.getDescricao().trim().equals("")) {
			throw new NegocioException("O nome do modelo nao pode ser nulo");
		}
		if(modeloCarro.getFabricante() == null) {
			throw new NegocioException("Informe o fabricante do carro");
		}

		this.modeloCarroDao.Salvar(modeloCarro);

	}

}
