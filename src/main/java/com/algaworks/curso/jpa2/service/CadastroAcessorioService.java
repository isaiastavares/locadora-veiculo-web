package com.algaworks.curso.jpa2.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.AcessorioDAO;
import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CadastroAcessorioService implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 475282884054858082L;
	@Inject
	private AcessorioDAO acessorioDao;

	@Transactional
	public void salvar(Acessorio acessorio) throws NegocioException {

		if(acessorio.getDescricao() == null || acessorio.getDescricao().trim().equals("")) {
			throw new NegocioException("Informe a descrição do acessório");
		}

		this.acessorioDao.salvar(acessorio);

	}

}
