package com.algaworks.curso.jpa2.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.FabricanteDAO;
import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CadastroFabricanteService implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 6265851737295063111L;
	@Inject
	private FabricanteDAO fabricanteDao;

	@Transactional //Abre a transacao e commita ou da rollback segundo evolucao da bargaca
	public void Salvar(Fabricante fabricante) throws NegocioException {

		if(fabricante.getNome() == null || fabricante.getNome().trim().equals("")) {
			throw new NegocioException("Nome do fabricante é obrigatório");
		}

		this.fabricanteDao.Salvar(fabricante);

	}

}
