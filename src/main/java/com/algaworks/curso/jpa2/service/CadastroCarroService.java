package com.algaworks.curso.jpa2.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.algaworks.curso.jpa2.dao.CarroDAO;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jpa.Transactional;

public class CadastroCarroService implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2946137637347371462L;
	@Inject
	private CarroDAO carroDao;

	@Transactional
	public void salvar(Carro carro) throws NegocioException {

		//Validações do cadastro
		carro.isCarroValido(carro);

		this.carroDao.salvar(carro);

	}

}
