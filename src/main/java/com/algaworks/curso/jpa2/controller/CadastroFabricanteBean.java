package com.algaworks.curso.jpa2.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.curso.jpa2.modelo.Fabricante;
import com.algaworks.curso.jpa2.service.CadastroFabricanteService;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroFabricanteBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8446540124580620802L;
	@Inject
	private CadastroFabricanteService cadastroFabricanteService;
	private Fabricante fabricante;

	/* Executa toda vez que esse bean e chamado */
	@PostConstruct
	public void init() {
		this.limpar();
	}

	/* Limpa os dados do formulario */
	public void limpar() {
		this.fabricante = new Fabricante();
	}

	public void salvar() {
		try {
			this.cadastroFabricanteService.Salvar(fabricante);
			FacesUtil.addSuccessMessage("Fabricante cadastrado com sucesso");

			this.limpar();
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}

	}

	public Fabricante getFabricante() {
		return fabricante;
	}
	public void setFabricante(Fabricante fabricante) {
		this.fabricante = fabricante;
	}
}
