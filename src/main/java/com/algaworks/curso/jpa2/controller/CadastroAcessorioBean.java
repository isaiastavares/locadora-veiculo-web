package com.algaworks.curso.jpa2.controller;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.curso.jpa2.modelo.Acessorio;
import com.algaworks.curso.jpa2.service.CadastroAcessorioService;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jsf.FacesUtil;

@Named
@ViewScoped
public class CadastroAcessorioBean implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3828746693423101180L;

	private Acessorio acessorio;

	@Inject
	private CadastroAcessorioService cadastroAcessorioService;


	public CadastroAcessorioBean() {
		this.limpar();
	}

	public void limpar() {
		acessorio = new Acessorio();
	}

	public void salvar() {

		try {
			this.cadastroAcessorioService.salvar(acessorio);
			FacesUtil.addSuccessMessage("Acessorio salvo com sucesso");
		} catch( NegocioException e ) {
			FacesUtil.addErrorMessage(e.getMessage());
		}

		this.limpar();
	}


	public Acessorio getAcessorio() {
		return acessorio;
	}

	public void setAcessorio(Acessorio acessorio) {
		this.acessorio = acessorio;
	}

}
