package com.algaworks.curso.jpa2.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.curso.jpa2.dao.CarroDAO;
import com.algaworks.curso.jpa2.modelo.Carro;
import com.algaworks.curso.jpa2.service.exception.NegocioException;
import com.algaworks.curso.jpa2.util.jsf.FacesUtil;

@Named
@ViewScoped
public class PesquisaCarroBean implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = 1860975630415350717L;

	private Carro carroSelecionado;

	private List<Carro> carros;

	@Inject
	private CarroDAO carroDao;

	@PostConstruct
	public void inicializar() {
		this.carros = this.carroDao.buscarTodos();
	}

	public void excluir() {
		try {
			this.carroDao.excluir(carroSelecionado);
			this.carros.remove(carroSelecionado);

			FacesUtil.addSuccessMessage("Carro excluido com sucesso");
		} catch (NegocioException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
	}

	public Carro getCarroSelecionado() {
		return carroSelecionado;
	}

	public void setCarroSelecionado(Carro carroSelecionado) {
		this.carroSelecionado = carroSelecionado;
	}

	public List<Carro> getCarros() {
		return carros;
	}

	public void buscarCarroComAcessorios() {
		carroSelecionado = carroDao.buscarCarroComAcessorios( carroSelecionado.getCodigo() );
	}

}
